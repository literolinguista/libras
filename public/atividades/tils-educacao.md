## Seminário de TILS...

> "... um interlocutor ativo" Lacerda.

Semiário sobre o tema "A influência do TILS (tradutor e intérprete de Libras) no processo de aprendizagem de educandos surdos" apresentado a disciplina de TILS da querida professora ["Nacláudia"](http://lattes.cnpq.br/5327432846770332 "Currículo Lattes da professora Ana Cláudia"){target="_blank"}.

- [Apresentação...](tils-educacao/tils-educacao.html "Apresentação..."){target="_blank"}
- [Roteiro da apresentação...](tils-educacao/textos/roteiro-tils-educacao.pdf "Roteiro da apresentação..."){target="_blank"}

[Voltar à página Libras...](../index.html)