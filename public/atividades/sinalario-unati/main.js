// Código do arcordeon

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

// Código do slideshow

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) { slideIndex = 1 }
  if (n < 1) { slideIndex = slides.length }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
}

// Código do menu para escolher vídeos
function displayVideo() {
  var selector = document.getElementById("video-selector");
  var selectedVideo = selector.value;
  var displayedVideo = document.getElementById("displayed-video");
  displayedVideo.src = selectedVideo;
  displayedVideo.type = "video/mp4";
  displayedVideo.controls = true;
}

// Código de avaliação dos vídeos
document.addEventListener('DOMContentLoaded', async function () {
  const supabaseUrl = 'https://aclthmpoqvjrovsocbuf.supabase.co';
  const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImFjbHRobXBvcXZqcm92c29jYnVmIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MzIxMzAzNjYsImV4cCI6MjA0NzcwNjM2Nn0.iMb6krW9HILff8eH5Kdxt2R_9UlvXsBI5MgHEEoRFzM';
  const supabase = supabase.createClient(supabaseUrl, supabaseKey);



  // Função para gerenciar o estado das interações do usuário
  const UserInteractions = {
    getInteractionState(videoId) {
      const stored = localStorage.getItem(`interaction_${videoId}`);
      return stored ? stored : null; // 'like', 'dislike', ou null
    },

    setInteractionState(videoId, state) {
      localStorage.setItem(`interaction_${videoId}`, state);
    },

    removeInteractionState(videoId) {
      localStorage.removeItem(`interaction_${videoId}`);
    }
  };

  function createInteractionButtons(videoId) {
    const interactionContainer = document.createElement('div');
    interactionContainer.innerHTML = `
          <div class="video-interactions" data-video-id="${videoId}">
            <button class="interaction-btn like-btn">
              👍 <span class="like-count">0</span>
            </button>
            <button class="interaction-btn dislike-btn">
              👎 <span class="dislike-count">0</span>
            </button>
          </div>
        `;
    return interactionContainer;
  }

  async function loadCounts(videoId) {
    const { data, error } = await supabase
      .from('video_interactions')
      .select('likes, dislikes')
      .eq('video_id', videoId)
      .single();

    if (error) {
      console.error('Error loading counts:', error);
      return { likes: 0, dislikes: 0 };
    }

    return data || { likes: 0, dislikes: 0 };
  }

  async function updateCounts(videoId, likes, dislikes) {
    const { error } = await supabase
      .from('video_interactions')
      .upsert({
        video_id: videoId,
        likes,
        dislikes,
        id: Date.now()
      });

    if (error) console.error('Error updating counts:', error);
  }

  // Função para atualizar a aparência dos botões baseado no estado
  function updateButtonStates(container, videoId) {
    const likeBtn = container.querySelector('.like-btn');
    const dislikeBtn = container.querySelector('.dislike-btn');
    const currentState = UserInteractions.getInteractionState(videoId);

    // Resetar classes
    likeBtn.classList.remove('active');
    dislikeBtn.classList.remove('active');

    // Aplicar classe active baseado no estado atual
    if (currentState === 'like') {
      likeBtn.classList.add('active');
    } else if (currentState === 'dislike') {
      dislikeBtn.classList.add('active');
    }
  }

  // Adicionar botões e eventos para cada vídeo
  document.querySelectorAll('.video-container').forEach(async (container) => {
    const videoId = container.querySelector('video')?.id || 'default';
    const buttons = createInteractionButtons(videoId);
    container.appendChild(buttons);

    const counts = await loadCounts(videoId);
    const likeCount = buttons.querySelector('.like-count');
    const dislikeCount = buttons.querySelector('.dislike-count');

    likeCount.textContent = counts.likes;
    dislikeCount.textContent = counts.dislikes;

    // Atualizar estado inicial dos botões
    updateButtonStates(buttons, videoId);

    buttons.querySelector('.like-btn').addEventListener('click', async () => {
      const currentState = UserInteractions.getInteractionState(videoId);
      let likes = parseInt(likeCount.textContent);
      let dislikes = parseInt(dislikeCount.textContent);

      if (currentState === 'like') {
        // Remover like
        likes--;
        UserInteractions.removeInteractionState(videoId);
      } else {
        // Adicionar like
        likes++;
        if (currentState === 'dislike') {
          dislikes--;
        }
        UserInteractions.setInteractionState(videoId, 'like');
      }

      likeCount.textContent = likes;
      dislikeCount.textContent = dislikes;
      updateButtonStates(buttons, videoId);
      await updateCounts(videoId, likes, dislikes);
    });

    buttons.querySelector('.dislike-btn').addEventListener('click', async () => {
      const currentState = UserInteractions.getInteractionState(videoId);
      let likes = parseInt(likeCount.textContent);
      let dislikes = parseInt(dislikeCount.textContent);

      if (currentState === 'dislike') {
        // Remover dislike
        dislikes--;
        UserInteractions.removeInteractionState(videoId);
      } else {
        // Adicionar dislike
        dislikes++;
        if (currentState === 'like') {
          likes--;
        }
        UserInteractions.setInteractionState(videoId, 'dislike');
      }

      likeCount.textContent = likes;
      dislikeCount.textContent = dislikes;
      updateButtonStates(buttons, videoId);
      await updateCounts(videoId, likes, dislikes);
    });
  });
});