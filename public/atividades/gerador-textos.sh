#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Uso: $0 <nome do arquivo>"
  exit 1
fi

arquivo="$1"
saida="${arquivo%.*}.html"
modelo="modelo-textos.html"

if [ ! -f "$arquivo" ]; then
  echo "Arquivo não encontrado: $arquivo" >&2
  exit 1
fi

if [ ! -f "$modelo" ]; then
  echo "Modelo não encontrado: $modelo" >&2
  exit 1
fi

pandoc -s "$arquivo" -o "$saida" --template "$modelo" 2> /dev/null

# O pandoc deixa algumas tags vazias, não descobri o motivo, mas o comando abaixo as remove

sed -i '/<[^\/>]*>\s*<\/[^>]*>/d' "$saida"

echo "Conversão completa: $saida"

