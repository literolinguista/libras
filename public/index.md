## LIBRAS - Língua brasileira de sinais

> "O silêncio só é abismo para os que escorregam o olhar das mãos que sinalizam do coração..."

### Atividades relacionadas à Libras. 

Atividades de Libras. Pode modificar, compartilhar, etc... mantendo a [Licença Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR "Licença Creative Commons"){target="_blank"}.

- [Conectando Machado, Djavan e o funk carioca...](atividades/cas/2AvaliacaoCAS.html "Conectando Machado, Djavan e o funk carioca..."){target="_blank"} (17.06.2024 - avaliação)
- [A influência do TILS no processo...](atividades/tils-educacao.html "A influência do TILS no processo de aprendizagem dos educandos surdos") (25.02.2024 - seminário)
- [Alfabeto manual](atividades/alfabeto-manual/index.html "Alfabeto manual"){target="_blank"} (01.04.2024 - atividade) 
- [Sinalário](atividades/sinalario/index.html "Sinalário"){target="_blank"} (01.04.2024 - rascunho de recurso) 
- [Fonologia de Libras](# "Fonologia de Libras") (em breve...)

[Voltar ao início...](https://literolinguista.gitlab.io/index.html)
